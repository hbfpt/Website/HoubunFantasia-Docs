import {
  EnvelopeIcon,
  AtSymbolIcon,
  DocumentDuplicateIcon,
  InboxArrowDownIcon,
} from "@heroicons/react/24/outline";
import { CopyToClipboard } from "react-copy-to-clipboard";
import React, { useEffect, useState } from "react";
import clsx from "clsx";
import styles from "./join-index.module.css";
import Translate, { translate } from '@docusaurus/Translate';
export default function HeroUpgrade(): JSX.Element {
  const MailCommand = "misaka10843@outlook.jp";
  const QQCommand = "3594254539";

  const [copied, setCopied] = useState(false);
  useEffect(() => {
    let t: NodeJS.Timeout;
    if (copied) {
      t = setTimeout(() => {
        setCopied(false);
      }, 3000);
    }
    return () => {
      t && clearTimeout(t);
    };
  }, [copied]);

  return (
    <section className="padding-vert--xl">
      <div className="container">
        <div className="row">
          <div className="col col--7 margin-vert--xl wow fadeIn slow">
            <h1 className={styles.slogan}>
              <span>
                <span className={styles.slogan__highlight}><Translate>加快</Translate></span> <Translate>我们的</Translate>
              </span>
              <span><Translate>游戏制作吧！</Translate></span>
            </h1>
            <p className={styles.description}>
              <Translate>如果您有任何一项技能，我们都十分欢迎您加入我们的团队！</Translate><br></br>
              <Translate>如果我们决定以买断制发布，您还可以获取分成！</Translate><br></br>
              <Translate>请不要害羞，现在就加入我们吧！</Translate><br></br>
            </p>
            <p className={styles.description}>
              <Translate>您可以在下方查询我们的职位！十分期待您的加入！</Translate>
            </p>
            <p className={styles.description}>
              <Translate>请注意！我们是为爱发电的团队，团队中的每一个小伙伴都是无私奉献</Translate><br></br>
              <Translate>您可以不用向上班一样，但是我们还是希望能多多参与</Translate><br></br>
              <Translate>如果您是希望找一份有收入的工作，那么我们可能不适合您</Translate>
            </p>
          </div>
          <div className="col col--1" />
          <div className="col col--4">
            <div className={styles.commands_wrapper}>
              <div className={clsx("wow fadeIn slow", styles.commands_container)}>
                <h3>
                  <EnvelopeIcon
                    className={styles.commands_icon}
                    stroke="currentColor"
                    aria-hidden="true"
                  />{" "}
                  Mail
                </h3>
                <CopyToClipboard
                  text={MailCommand}
                  onCopy={() => {
                    setCopied(true);
                  }}
                >
                  <button
                    className={clsx(
                      "button button--lg button--block button--outline button--secondary",
                      styles.command
                    )}
                  >
                    <span className={styles.command__text}>
                      {MailCommand}
                    </span>
                    <DocumentDuplicateIcon className={styles.command__icon} />
                  </button>
                </CopyToClipboard>
              </div>
              <div className={clsx("wow fadeIn slow", styles.commands_container)}>
                <h3>
                  <AtSymbolIcon className={styles.commands_icon} stroke="currentColor" aria-hidden="true" />
                  QQ（<Translate>请备注来意哦qwq</Translate>）
                </h3>
                <CopyToClipboard
                  text={QQCommand}
                  onCopy={() => {
                    setCopied(true);
                  }}
                >
                  <button
                    className={clsx(
                      "button button--lg button--block button--outline button--secondary",
                      styles.command
                    )}
                  >
                    <span className={styles.command__text}>
                      {QQCommand}
                    </span>
                    <DocumentDuplicateIcon className={styles.command__icon} />
                  </button>
                </CopyToClipboard>
              </div>
              <div className={clsx("wow fadeIn slow delay-1s", styles.reaching_out)}>
                <h4><Translate>不知道怎么单独联系?</Translate></h4>
                <p>
                  <Translate>那就去我们的社群/社区/网站/仓库来联系我们吧！</Translate><br></br>
                  <Translate>请注意要@管理员哦qwq</Translate>
                </p>
                <div className={styles.reaching_out__link_container}>
                  <a href="https://jq.qq.com/?_wv=1027&k=Hg0RpVVI" target="_blank">

                    <svg style={{ height: "1rem", width: "1rem" }}
                      fill="currentColor"
                      role="img"
                      viewBox="0 0 1024 1024"
                      xmlns="http://www.w3.org/2000/svg"
                      className="margin-right--sm"><path d="M824.8 613.2c-16-51.4-34.4-94.6-62.7-165.3C766.5 262.2 689.3 112 511.5 112 331.7 112 256.2 265.2 261 447.9c-28.4 70.8-46.7 113.7-62.7 165.3-34 109.5-23 154.8-14.6 155.8 18 2.2 70.1-82.4 70.1-82.4 0 49 25.2 112.9 79.8 159-26.4 8.1-85.7 29.9-71.6 53.8 11.4 19.3 196.2 12.3 249.5 6.3 53.3 6 238.1 13 249.5-6.3 14.1-23.8-45.3-45.7-71.6-53.8 54.6-46.2 79.8-110.1 79.8-159 0 0 52.1 84.6 70.1 82.4 8.5-1.1 19.5-46.4-14.5-155.8z" p-id="2671"></path></svg>
                    <Translate>QQ群</Translate>
                  </a>
                  <a href="https://twitter.com/kaosu_sensei" target="_blank">
                    <svg
                      style={{ height: "1rem", width: "1rem" }}
                      fill="currentColor"
                      role="img"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                      className="margin-right--sm"
                    >
                      <title>Twitter</title>
                      <path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z" />
                    </svg>
                    Twitter
                  </a>
                  <a href="https://gitlab.com/hbfpt" target="_blank">
                    <svg
                      style={{ height: "1rem", width: "1rem" }}
                      fill="currentColor"
                      role="img"
                      viewBox="0 0 448 512"
                      xmlns="http://www.w3.org/2000/svg"
                      className="margin-right--sm"
                    >
                      <title>Gitlab</title>
                      <path xmlns="http://www.w3.org/2000/svg" d="M48 32H400C426.5 32 448 53.5 448 80V432C448 458.5 426.5 480 400 480H48C21.5 480 0 458.5 0 432V80C0 53.5 21.5 32 48 32zM382.1 224.9L337.5 108.5C336.6 106.2 334.9 104.2 332.9 102.9C331.3 101.9 329.5 101.3 327.7 101.1C325.9 100.9 324 101.2 322.3 101.8C320.6 102.5 319 103.5 317.8 104.9C316.6 106.3 315.7 107.9 315.2 109.7L285 201.9H162.1L132.9 109.7C132.4 107.9 131.4 106.3 130.2 104.9C128.1 103.6 127.4 102.5 125.7 101.9C123.1 101.2 122.1 100.1 120.3 101.1C118.5 101.3 116.7 101.9 115.1 102.9C113.1 104.2 111.5 106.2 110.6 108.5L65.94 224.9L65.47 226.1C59.05 242.9 58.26 261.3 63.22 278.6C68.18 295.9 78.62 311.1 92.97 321.9L93.14 322L93.52 322.3L161.4 373.2L215.6 414.1C217.1 415.1 220.9 416.9 223.9 416.9C226.9 416.9 229.9 415.1 232.3 414.1L286.4 373.2L354.8 322L355 321.9C369.4 311 379.8 295.8 384.8 278.6C389.7 261.3 388.1 242.9 382.5 226.1L382.1 224.9z" />
                    </svg>
                    Gitlab
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
