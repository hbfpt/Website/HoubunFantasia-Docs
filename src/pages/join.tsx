import JoinIndex from "@site/src/components/join/join-index";
import JoinPosition from "@site/src/components/join/join-position";
import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import Head from '@docusaurus/Head';
import Translate, { translate } from '@docusaurus/Translate';

export default function Upgrade(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout title={
      translate({
        id: "join.title",
        message: '加入我们',
      })
    }
      description={
        translate({
          id: "join.description",
          message: '加快 我们的游戏制作吧！',
        })
      }>
      <Head>
        <script>new WOW().init();</script>
      </Head>
      <main>
        <JoinIndex />
        <JoinPosition />
      </main>
    </Layout>
  );
}
