import FreeGetIndex from "@site/src/components/free-get/free-get-index";
import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import Head from '@docusaurus/Head';
import Translate, { translate } from '@docusaurus/Translate';
export default function Upgrade(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout title={
      translate({
        id: "freeGet.title",
        message: '免费激活码',
      })
    }
      description={
        translate({
          id: "freeGet.description",
          message: '免费 获取游戏激活码！',
        })
      }>
      <Head>
        <script>new WOW().init();</script>
      </Head>
      <main>
        <FreeGetIndex />
      </main>
    </Layout>
  );
}
