import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HeroLerna from "@site/src/components/index/first-element";
import ProjectsUsingLerna from "@site/src/components/index/get-news";
import PublishWithLerna from "@site/src/components/index/second-card";
import PoweredByNx from "@site/src/components/index/first-card";
import AboutLerna from "@site/src/components/index/about";
import Head from '@docusaurus/Head';
import ReactPlayer from 'react-player'
import Translate, { translate } from '@docusaurus/Translate';

// Render a YouTube video player

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (

    <Layout
      title="Kirara！"
      description={
        translate({
          id: "index.description",
          message: '致力于在2D中还原芳文社Manga Time Kirara系列漫画中各个场景/角色/工作/功能/剧情的同人游戏',
        })
      }
    >
      <Head>
        <script>new WOW().init();</script>
      </Head>
      <main>
        <HeroLerna />
        <ProjectsUsingLerna />
        <PoweredByNx />
        <PublishWithLerna />
        <AboutLerna />
      </main>
    </Layout>
  );
}
