---
id: audiomanager
title: 音频管理器
type: GameDev
---
此组件为管理所有音频，控制音频播放与音量的组件

## 使用方法

在 `AudioManger`中，我们提供了三种控制，控制**播放的音频**，控制**音频的停止**，与**控制音频的音量**

| 函数                                      | 简介                   |
| ----------------------------------------- | ---------------------- |
| `PlayBGM(string name, bool loop)`       | 控制BGM的播放与循环    |
| `PlayVoice(string name, bool loop)`     | 控制Voice的播放与循环  |
| `PlayEffect(string name, bool loop)`    | 控制Effect的播放与循环 |
| `SetVolume(string Group, float volume)` | 控制各个属性的音量     |
| `BGMSource`                             | 直接控制BGM的Source    |
| `EffectSource`                          | 直接控制Effect的Source |
| `VoiceSource`                           | 直接控制Voice的Source  |

### 控制播放的音频

`AudioManger.Instance.PlayBGM("title",true);`

其中，`PlayBGM`为分类

我们暂时先提供了三种类别的音频控制： `PlayBGM`，`PlayVoice`，`PlayEffect`

这三个函数共有以下参数

| 参数 | 简介                               | 值属性 | 可用值             |
| ---- | ---------------------------------- | ------ | ------------------ |
| name | 从管理器中寻找与之相对应名称的音频 | string | 依靠脚本           |
| loop | 是否循环播放                       | bool   | `false`/`true` |

### 控制音频的音量

`AudioManger.Instance.SetVolume(MainVolume, 20f);`

其中，`BGM`为分类

我们提供了以下参数

| 参数   | 简介       | 值属性 | 可用值                                                             |
| ------ | ---------- | ------ | ------------------------------------------------------------------ |
| Group  | 控制指定组 | string | `MainVolume`/`BGMVolume`/`VoiceVolume`/`SoundEffectVolume` |
| volume | 音量调整   | float  | 任何float(理论)                                                    |

## 如何添加新音频

我们使用了csv文件来存储音频路径与名字，还有加载格式

大概格式如下：

| col1     | col2                   | 是否使用 Addressables 进行加载 |
| -------- | ---------------------- | ------------------------------ |
| titleBGM | AudioClip/UI/sc_title  | false                          |
| endBGM   | AudioClip/UI/sn_ending | false                          |

注意！因为**Addressables**的加载问题，场景加载时不会立即播放音频，从而加快场景加载速度。只有在音频加载完成后，才会开始播放音频，确保音频播放的流畅性和稳定性。

所以如果需要加载完成后立即播放音频的话，请将 `是否使用 Addressables 进行加载`设置为 `false`，否则强烈建议设置为 `true`

### 如何导入音频

因为 `Addressables`的原因，需要特殊方式导入，如果不使用 `Addressables`可以直接跳转到**普通加载**部分

#### 使用Addressables加载

在使用**Addressables**导入音频时，请注意先导入进入 `Assets/Resources/AudioClip/子文件夹`，然后再点击音频文件，选择并打开 `Addressables`选项后点击**yes**

![1678029003094](image/AudioManager/1678029003094.png)

然后点击 `窗口->资产管理->Addressables->组`打开资源节目，然后复制里面刚刚导入的音频的路径即可

![1678028974256](image/AudioManager/1678028974256.png)

#### 使用 `Resources.Load`加载(普通加载)

直接将音频导入进 `Assets/Resources/AudioClip/子文件夹`中即可

### 如何编写csv文件

(在VScode等高级编辑器中，可以添加插件实现csv表格编辑，而不是下面这种原字符串编辑)

csv文件在 `Assets/Resources/AudioClip/AudioList.csv`，打开后可以看见类似这样的结构

```plaintext
titleBGM,AudioClip/UI/sc_title,false
endBGM,AudioClip/UI/sn_ending,false
```

这种结构以半角符号 `,`为分割线，来制作类似表格的，转换过来就是类似[这个格式下](#如何添加新音频)的

所以每添加一个音频，就应该在文件后新建一行，并且填写 `<名称>,<路径>,<是否使用 Addressables 进行加载>`

请注意！如果是否使用 Addressables 进行加载，请按照[使用 Addressables 进行加载](#使用addressables加载)中的步骤复制到路径后填写进去(因为Addressables加载时是使用专门的文件夹中读取)
