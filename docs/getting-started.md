---
id: getting-started
title: Getting Started
type: tutorial
---
(#`O′)喂！游戏都还没开发到一半怎么有这种教程呢！

但是！

我们还是提供了一个渠道，可以来游玩我们的超超超早期的版本（其实现在就处于这种版本）

您可以在[这里](/docs/help/version)获取到所有版本的下载链接！
